#!/bin/bash

# Builds the dependencies needed for compiling Blender 2.53 on RHEL 6
#
# The process this script uses is:
# a) Firstly, use yum to install a list of packages known to be needed,
#    and which are available in the RHEL 6 repositories
#
# b) Then download the source RPMS for other needed packages, using ones known
#    to work.  (From Fedora 13 mostly, and a few from Fedora 12)
#
# c) Compile and install these downloaded SRPMS, in an order known to work ok
#    (tested on my boxes)
#
#   Reference URL where the initial list of packages was gotten from, then modified
#   to suit RHEL 6:
#     http://wiki.blender.org/index.php/Dev:2.5/Doc/Building_Blender/Linux/Fedora_12/CMake
#
# NOTE - You'll need to run this script with sudo (preferred) or as root, as it needs to
#        install packages.  Please inspect this script before running it, and change
#        anything you need.  At least the Fedora mirror server variable, and probably
#        the temp directory.  Maybe even the compilation flags too. :)

# History
# ***************************************
#
# 1.00          2010-08-29      Justin Clift
#               Initial version
#
# 1.01          2010-08-29      Justin Clift
#               Added the OpenEXR, OpenEXR-devel, OpenEXR-libs, and cmake packages
#

# Temporary directory that rpms will be downloaded into
FEDORA_MIRROR_DIR="http://mirror.internode.on.net/pub/fedora/linux"
TEMP_DIR="/tmp/blender_srpms"

# Compiler flags we'll be using for everything except "orc" (which --fast-math breaks)
# From here: http://durian.blender.org/development/system-benchmarks/
export OUR_CFLAGS="-O2 -mtune=native -fomit-frame-pointer -msse -msse2 -msse3 -ftree-vectorize -finline-functions -funswitch-loops -pipe --fast-math"

# Compiler flags for "orc" (same as above, minus --fast-math)
export ORC_ONLY_CFLAGS="-O2 -mtune=native -fomit-frame-pointer -msse -msse2 -msse3 -ftree-vectorize -finline-functions -funswitch-loops -pipe"

# Install packages mentioned on the Blender page and elsewhere
yum install -y systemtap-sdt-devel libffi-devel db4-devel sqlite-devel tix-devel tk-devel tcl-devel libGL-devel expat-devel gdbm-devel gmp-devel readline-devel openssl-devel ncurses-devel bzip2-devel

# Install packages found to be needed when compiling either blender or the source rpms later on
yum install -y nasm alsa-lib-devel libraw1394-devel libiec61883-devel libavc1394-devel libsndfile-devel doxygen byacc xmlto pulseaudio-libs-devel automake14 autoconf213 libXi-devel libXt-devel libsysfs-devel esound-devel arts-devel libogg-devel libvorbis-devel texi2html speex-devel SDL-devel openjpeg-devel libtheora-devel libdc1394-devel gsm-devel php-cli cppunit-devel graphviz-devel gtk-doc libsamplerate-devel fftw-devel gettext-devel libudev-devel rpm-build OpenEXR OpenEXR-devel OpenEXR-libs cmake


## Download things

mkdir "$TEMP_DIR"
cd "$TEMP_DIR"

# Standard Fedora open source things
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/dirac-1.0.2-3.fc12.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/freealut-1.1.0-11.fc13.src.rpm	
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/id3lib-3.8.3-24.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/jack-audio-connection-kit-0.118.0-1.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/js-1.70-10.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/libfreebob-1.0.11-6.fc12.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/libmikmod-3.2.0-9.beta2.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/libmp4v2-1.5.0.1-10.fc12.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/libvdpau-0.4-1.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/glib-1.2.10-33.fc12.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/gtk+-1.2.10-70.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/mikmod-3.2.2-10.beta1.fc11.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/openal-soft-1.12.854-1.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/portaudio-19-9.fc12.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/xmms-1.2.11-10.20071117cvs.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/releases/13/Everything/source/SRPMS/yasm-0.8.0-3.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/glew-1.5.4-1.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/libvpx-0.9.1-2.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/nasm-2.07-4.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/orc-0.4.6-1.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/schroedinger-1.0.9-2.fc13.src.rpm
wget $FEDORA_MIRROR_DIR/updates/13/SRPMS/wxGTK-2.8.11-1.fc13.src.rpm

# From the rpmfusion "free" repository
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/a52dec-0.7.4-15.fc11.src.rpm
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/faad2-2.7-1.fc11.src.rpm
wget http://download1.rpmfusion.org/free/fedora/updates/13/SRPMS/ffmpeg-0.6-3.fc13.src.rpm
wget http://download1.rpmfusion.org/free/fedora/updates/13/SRPMS/gpac-0.4.6-0.9.cvs20100527.fc13.src.rpm
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/lame-3.98.3-1.fc13.src.rpm
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/libmad-0.15.1b-13.fc12.src.rpm
wget http://download1.rpmfusion.org/free/fedora/updates/13/SRPMS/libva-freeworld-0.31.1-1.sds4.fc13.src.rpm
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/x264-0.0.0-0.27.20100130git3659b81.fc13.src.rpm
wget http://download1.rpmfusion.org/free/fedora/releases/13/Everything/source/SRPMS/xvidcore-1.2.1-3.fc12.src.rpm

# From the rpmfusion "non-free" repository
wget http://download1.rpmfusion.org/nonfree/fedora/releases/13/Everything/source/SRPMS/faac-1.28-2.fc12.src.rpm


# Install the downloaded rpms so they can be built
rpm -ivh libmp4v2-1.5.0.1-10.fc12.src.rpm
rpm -ivh xmms-1.2.11-10.20071117cvs.fc13.src.rpm
rpm -ivh gtk+-1.2.10-70.fc13.src.rpm
rpm -ivh glib-1.2.10-33.fc12.src.rpm
rpm -ivh mikmod-3.2.2-10.beta1.fc11.src.rpm
rpm -ivh libmikmod-3.2.0-9.beta2.fc13.src.rpm
rpm -ivh libvpx-0.9.1-2.fc13.src.rpm
rpm -ivh schroedinger-1.0.9-2.fc13.src.rpm
rpm -ivh orc-0.4.6-1.fc13.src.rpm
rpm -ivh glew-1.5.4-1.fc13.src.rpm
rpm -ivh libvdpau-0.4-1.fc13.src.rpm
rpm -ivh dirac-1.0.2-3.fc12.src.rpm
rpm -ivh freealut-1.1.0-11.fc13.src.rpm	
rpm -ivh jack-audio-connection-kit-0.118.0-1.fc13.src.rpm
rpm -ivh libfreebob-1.0.11-6.fc12.src.rpm
rpm -ivh openal-soft-1.12.854-1.fc13.src.rpm
rpm -ivh portaudio-19-9.fc12.src.rpm
rpm -ivh yasm-0.8.0-3.fc13.src.rpm
rpm -ivh id3lib-3.8.3-24.fc13.src.rpm
rpm -ivh js-1.70-10.fc13.src.rpm
rpm -ivh wxGTK-2.8.11-1.fc13.src.rpm
rpm -ivh nasm-2.07-4.fc13.src.rpm
rpm -ivh ffmpeg-0.6-3.fc13.src.rpm
rpm -ivh libva-freeworld-0.31.1-1.sds4.fc13.src.rpm
rpm -ivh lame-3.98.3-1.fc13.src.rpm
rpm -ivh x264-0.0.0-0.27.20100130git3659b81.fc13.src.rpm
rpm -ivh gpac-0.4.6-0.9.cvs20100527.fc13.src.rpm
rpm -ivh a52dec-0.7.4-15.fc11.src.rpm
rpm -ivh faad2-2.7-1.fc11.src.rpm
rpm -ivh libmad-0.15.1b-13.fc12.src.rpm
rpm -ivh xvidcore-1.2.1-3.fc12.src.rpm
rpm -ivh faac-1.28-2.fc12.src.rpm


# Change into the $HOME/rpmbuild/SPEC directory, then build and install the source rpms
cd "$HOME/rpmbuild/SPECS"

# Set the compiler flags
export CFLAGS="$OUR_CFLAGS"
export CPPFLAGS="$CFLAGS"

# Python 3
rpm -bb python3.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/python3-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-libs-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-devel-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-tools-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-tkinter-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-test-3.1.2-4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/python3-debuginfo-3.1.2-4.el6.x86_64.rpm

rpmbuild -bb xvidcore.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/xvidcore-1.2.1-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xvidcore-devel-1.2.1-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xvidcore-debuginfo-1.2.1-3.el6.x86_64.rpm

rpmbuild -bb libfreebob.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libfreebob-1.0.11-6.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libfreebob-devel-1.0.11-6.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libfreebob-debuginfo-1.0.11-6.el6.x86_64.rpm

rpmbuild -bb jack-audio-connection-kit.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/jack-audio-connection-kit-0.118.0-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/jack-audio-connection-kit-devel-0.118.0-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/jack-audio-connection-kit-example-clients-0.118.0-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/jack-audio-connection-kit-debuginfo-0.118.0-1.el6.x86_64.rpm

rpmbuild -bb yasm.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/yasm-0.8.0-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/yasm-devel-0.8.0-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/yasm-debuginfo-0.8.0-3.el6.x86_64.rpm

rpmbuild -bb --define '_without_gpac=1' x264.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/x264-0.0.0-0.27.20100130git3659b81.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/x264-libs-0.0.0-0.27.20100130git3659b81.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/x264-devel-0.0.0-0.27.20100130git3659b81.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/x264-debuginfo-0.0.0-0.27.20100130git3659b81.el6.x86_64.rpm

rpmbuild -bb portaudio.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/portaudio-19-9.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/portaudio-devel-19-9.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/portaudio-debuginfo-19-9.el6.x86_64.rpm

rpmbuild -bb openal-soft.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/openal-soft-1.12.854-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/openal-soft-devel-1.12.854-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/openal-soft-debuginfo-1.12.854-1.el6.x86_64.rpm

rpmbuild -bb glib.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/glib-1.2.10-33.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/glib-devel-1.2.10-33.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/glib-debuginfo-1.2.10-33.el6.x86_64.rpm

rpmbuild -bb gtk+.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/gtk+-1.2.10-70.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/gtk+-devel-1.2.10-70.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/gtk+-debuginfo-1.2.10-70.el6.x86_64.rpm

rpmbuild -bb lame.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/lame-3.98.3-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/lame-libs-3.98.3-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/lame-devel-3.98.3-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/lame-mp3x-3.98.3-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/lame-debuginfo-3.98.3-1.el6.x86_64.rpm

rpmbuild -bb freealut.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/freealut-1.1.0-11.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/freealut-devel-1.1.0-11.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/freealut-debuginfo-1.1.0-11.el6.x86_64.rpm

rpmbuild -bb libmp4v2.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libmp4v2-1.5.0.1-10.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libmp4v2-devel-1.5.0.1-10.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libmp4v2-debuginfo-1.5.0.1-10.el6.x86_64.rpm

rpmbuild -bb faac.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/faac-1.28-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/faac-devel-1.28-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/faac-debuginfo-1.28-2.el6.x86_64.rpm

rpmbuild -bb id3lib.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/id3lib-3.8.3-24.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/id3lib-devel-3.8.3-24.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/id3lib-debuginfo-3.8.3-24.el6.x86_64.rpm

rpmbuild -bb libmikmod.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libmikmod-3.2.0-9.beta2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libmikmod-devel-3.2.0-9.beta2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libmikmod-debuginfo-3.2.0-9.beta2.el6.x86_64.rpm

rpmbuild -bb mikmod.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/mikmod-3.2.2-10.beta1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/mikmod-debuginfo-3.2.2-10.beta1.el6.x86_64.rpm

rpmbuild -bb xmms.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/xmms-1.2.11-10.20071117cvs.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xmms-libs-1.2.11-10.20071117cvs.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xmms-esd-1.2.11-10.20071117cvs.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xmms-devel-1.2.11-10.20071117cvs.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xmms-debuginfo-1.2.11-10.20071117cvs.el6.x86_64.rpm

rpmbuild -bb faad2.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/faad2-2.7-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/faad2-libs-2.7-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/faad2-devel-2.7-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/xmms-faad2-2.7-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/faad2-debuginfo-2.7-1.el6.x86_64.rpm

rpmbuild -bb libvpx.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libvpx-0.9.1-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvpx-devel-0.9.1-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvpx-utils-0.9.1-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvpx-debuginfo-0.9.1-2.el6.x86_64.rpm

rpmbuild -bb dirac.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/dirac-1.0.2-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/dirac-libs-1.0.2-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/dirac-devel-1.0.2-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/dirac-docs-1.0.2-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/dirac-debuginfo-1.0.2-3.el6.x86_64.rpm

rpmbuild -bb libvdpau.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libvdpau-0.4-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvdpau-docs-0.4-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvdpau-devel-0.4-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libvdpau-debuginfo-0.4-1.el6.x86_64.rpm

rpmbuild -bb libva-freeworld.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/libva-freeworld-0.31.1-1.sds4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libva-freeworld-devel-0.31.1-1.sds4.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/libva-freeworld-debuginfo-0.31.1-1.sds4.el6.x86_64.rpm

rpmbuild -bb glew.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/glew-1.5.4-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/glew-devel-1.5.4-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/glew-debuginfo-1.5.4-1.el6.x86_64.rpm

# Change the compiler flags for the build of orc, otherwise it fails
# (on this version at least)
export CFLAGS="$ORC_ONLY_CFLAGS"
export CPPFLAGS="$CFLAGS"
rpmbuild -bb orc.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/orc-0.4.6-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/noarch/orc-doc-0.4.6-1.el6.noarch.rpm $HOME/rpmbuild/RPMS/x86_64/orc-devel-0.4.6-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/orc-compiler-0.4.6-1.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/orc-debuginfo-0.4.6-1.el6.x86_64.rpm

# Set the compiler flags again
export CFLAGS="$OUR_CFLAGS"
export CPPFLAGS="$CFLAGS"
rpmbuild -bb schroedinger.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/schroedinger-1.0.9-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/schroedinger-devel-1.0.9-2.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/schroedinger-debuginfo-1.0.9-2.el6.x86_64.rpm

# The name of the "libva" package has changed to "libva-freeworld".  We adjust the ffmpeg package SPEC file before building with it
sed -i -e 's/^BuildRequires:  libva-devel/BuildRequires:  libva-freeworld-devel/' ffmpeg.spec
rpmbuild -bb ffmpeg.spec
yum install --nogpgcheck -y $HOME/rpmbuild/RPMS/x86_64/ffmpeg-0.6-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/ffmpeg-libs-0.6-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/ffmpeg-devel-0.6-3.el6.x86_64.rpm $HOME/rpmbuild/RPMS/x86_64/ffmpeg-debuginfo-0.6-3.el6.x86_64.rpm


# Done.  Now you're ready to compile Blender 2.53
echo
echo *****************************************
echo Done.  If the script got this far, then
echo you should be ok to build the Blender.
echo ie. using cmake, following the instructions
echo on the Blender page mentioned at the top of
echo this script.
echo *****************************************
echo

