[% topdir = "../.." -%]
[% PROCESS globals -%]
[% WRAPPER page
   title = "How to check if your hardware supports virtualization"
   h1 = "How to check if your hardware supports virtualization"
   section = "learning"
%]

[% WRAPPER h2 h2="Hardware virtualization technologies" anchor="hvm" %]

<p>
The first machines to support virtualization commercially were IBM
mainframes in the 1970s.  It has taken PCs nearly 40 years to catch
up.
</p>

<p>
Until a few years ago the central processors in PCs were not
<a href="http://en.wikipedia.org/wiki/Popek_and_Goldberg_virtualization_requirements">virtualizable</a>
except by using slow emulation or tricks.  However both AMD and Intel
have recently produced processors which include extra modes and
instructions that allow full, hardware-accelerated virtualization.
</p>

<p>
AMD and Intel's virtualization techniques are different and
incompatible, but all hypervisors hide the differences from you.
</p>

<p>
There are also several <q>generations</q> of virtualization
technologies available.  Initial implementations from both AMD and
Intel were quite slow, and great improvements have been made over
time.  These differences are not very visible to the end user, but we
cover some of the virtualization flags that you might see below.  The
upshot is that more modern processors are not only faster, but the
virtualization will be more efficient too.
</p>

[% END %]

[% WRAPPER h2 h2="Model numbers" anchor="model-numbers" %]

<p>
A good start is to look up the model number of your CPU on
the manufacturer's site.  The accurate model name and number should
be in <code>/proc/cpuinfo</code> after boot, and you
can look these up on the following pages:
</p>

<ul>
<li> <a href="http://ark.intel.com/">Intel ARK</a> </li>
<li> <a href="http://products.amd.com/">AMD Product Central</a> </li>
</ul>

<p>
For example, my
laptop's <a href="http://ark.intel.com/Product.aspx?id=43560">Intel i7
620M page is here</a>.
</p>

<p>
The Intel site is the most useful, and will tell you directly if the
processor supports virtualization (Intel VT-x) and/or secure
passthrough of PCI devices to guests (Intel VT-d).  The AMD site is
less than useful in this respect.  I have noticed some inaccuracies on
the Intel site.
</p>

[% END %]

[% WRAPPER h2 h2="<code>/proc/cpuinfo</code> flags" anchor="cpu-flags" %]

<p>
<code>/proc/cpuinfo</code> will tell you if the processor
supports virtualization <i>and</i> if it is enabled (but
it could not show up in flags because it is disabled &mdash;
more on this below).
</p>

<p>
The flags to look out for are:
</p>

<ul>
<li> <code>vmx</code> &mdash; Intel VT-x, basic virtualization </li>
<li> <code>svm</code> &mdash; AMD SVM, basic virtualization </li>
<li> <code>ept</code> &mdash; Extended Page Tables, an
Intel feature to make emulation of guest page tables faster. </li>
<li> <code>vpid</code> &mdash; VPID, an Intel feature to
make expensive TLB flushes unnecessary when context switching
between guests. </li>
<li> <code>npt</code> &mdash; AMD Nested Page Tables, similar
to EPT. </li>
<li> <code>tpr_shadow</code> and <code>flexpriority</code> &mdash;
Intel feature that reduces calls into the hypervisor when accessing
the Task Priority Register, which helps when running
certain types of SMP guests. </li>
<li> <code>vnmi</code> &mdash; Intel Virtual NMI feature which
helps with certain sorts of interrupt events in guests. </li>
</ul>

[% END %]

[% WRAPPER h2 h2="Kernel messages (dmesg) and the BIOS" anchor="dmesg" %]

<p>
The next thing to look at are the kernel messages by running
the command:
</p>

<pre>
dmesg | less
</pre>

<p>
The two messages to look out for are:
</p>

<pre>
kvm: no hardware support
</pre>

<p>
and
</p>

<pre>
kvm: disabled by bios
</pre>

<p>
<strong>Note:</strong> You will only see these messages if the
KVM kernel module has tried to load.  That is, if you tried
to start a guest already, or if you tried to load the KVM
module by hand (see <a href="#kvm-module">the next section</a>).
</p>

<p>
The <q>no hardware support</q> message is self-explanatory.
The <q>disabled by bios</q> message requires some explanation.  When
virtualization first came to PCs there was a worry that malware (on
Windows of course) might use the virtualization feature in order to
sit under the operating system like a hypervisor, and become
especially hard to remove.  Therefore virtualization can be completely
disabled at boot time, requiring a reboot and manual BIOS
configuration to enable it, which of course malware would not be able
to do.  This plan is good in theory, but it discounts one factor: the
inability of motherboard manufacturers to create BIOSes that aren't
full of bugs.  You may find that even though your processor supports
virtualization, it is not featured in the BIOS (hence impossible to
enable), or the BIOS may contain bugs that prevent you from enabling
it.  There's not much you can do in these situations except to try
updating the BIOS or replacing the motherboard.
</p>

[% END %]

[% WRAPPER h2 h2="Loading the KVM module" anchor="kvm-module" %]

<p>
You can try to load the KVM module by hand.
</p>

<p>
As explained above, there are two different brands of virtualization
(from Intel and AMD) which are incompatible.  Therefore KVM has
separate device drivers for each.
</p>

<p>
To load KVM on an Intel processor:
</p>

<pre>
modprobe kvm_intel
</pre>

<p>
To load KVM on an AMD processor:
</p>

<pre>
modprobe kvm_amd
</pre>

<p>
Check <code>dmesg</code> output after this to see if there
were any problems.  Check <code>lsmod</code> to see if the
modules were loaded.
</p>

[% END %]

[% END -%]
