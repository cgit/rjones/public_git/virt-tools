[% topdir = "../.." -%]
[% PROCESS globals -%]
[% WRAPPER page
   title = "Names and terms used in the virt tools documentation"
   h1 = "Names and terms used in the virt tools documentation"
   section = "learning"
   local_stylesheet = 1
%]

[% WRAPPER h2 h2="Hosts, guests, virtual machines, hypervisors" anchor="names" %]

<p>
The virt tools and
<a href="[% topdir %]/about/#projects">project documentation</a>
uses the terms
<strong>guest</strong>,
<strong>virtual machine</strong>,
<strong>VM</strong> and
<strong>domain</strong> interchangably to refer to a virtual machine.
The term <q>domain</q> comes from Xen.
</p>

<p>
When we say <strong>host</strong> we mean the ordinary physical machine
and the operating system which contains the guests.
</p>

<p>
The term <strong>hypervisor</strong> has several different meanings:
</p>

<ul>
<li> It can refer to the virtualization technology being employed.
For example you can discuss the pros and cons of VMWare vs
KVM as a <strong>hypervisor technology</strong>. </li>
<li> For some virtualization systems (notably Xen) it can
mean a separate component (the <strong>Xen hypervisor</strong>)
which provides virtualization.  In Xen, unlike other systems,
the host runs on top of the hypervisor and behaves more like a
specialized guest. </li>
<li> For KVM, you can refer to <q>Linux as the hypervisor</q>,
meaning that the ordinary Linux host kernel has extra hypervisor
capabilities provided by the KVM module. </li>
</ul>

<p>
Xen (only) uses the special terms <strong>Dom0</strong> (a.k.a.
<strong>Domain-0</strong>) and <strong>DomU</strong> as synonyms
for host and guest respectively.
</p>

[% END %]

[% WRAPPER h2 h2="Full virtualization and paravirtualization" anchor="fvpv" %]

<p class="vtmarginnote">
Examples of full virtualization technologies:
Bochs, QEMU, KVM, VMWare, Xen-FV, MAME.
</p>

<p>
<strong>Full virtualization</strong> (or <strong>fullvirt</strong>,
<strong>FV</strong>) is what you use to run an unmodified operating
system as a virtual machine.  The operating system <q>thinks</q> it is
running on a PC, and (from the point of view of the guest) it sees
usual hardware like a keyboard, disk drive, monitor and so on.  These
of course are not real, but are emulated by the hypervisor technology
underneath.
</p>

<p>
Full virtualization is usually a bit slower, because of all the
emulation, but it has the big advantage that you can run unmodified
operating systems like Windows.
</p>

<p>
<strong>Paravirtualization</strong>
(or <strong>paravirt</strong>, <strong>PV</strong>) is where you
modify the guest operating system so that it knows it is running in a
virtual machine and it cooperates with the host.  This is done to gain
extra performance by avoiding unnecessary emulation.
</p>

<p>
There are essentially two methods currently in use for
paravirtualization.  The <strong>Xen paravirt</strong> approach makes
deep changes to the Linux kernel so that cooperation is achieved at
many levels.  The advantage of Xen paravirt is
that <a href="#hardware">hardware support</a> is not required, but the
disadvantage is it is only possible for some Linux guests.
</p>

<p class="vtmarginnote">
Examples of FV with PV drivers technologies:
KVM Virtio, VMWare Tools, VirtualBox Additions, Xen-PV drivers.
</p>

<p>
The more common approach these days is <strong>device driver
paravirtualization</strong> where only the device drivers of the
operating system are replaced.  This is sometimes called
<strong>fullvirt with PV drivers</strong>.  This gains almost all the
performance benefits of paravirtualization, it works with Windows
guests, and it can be implemented incrementally by starting with a
fullvirt guest and adding PV drivers as required.  The downside is
that you need hardware support.  The most commonly paravirtualized
devices are: the hard disk (often called the <q>block device</q>), the
network device, the console, and the clock.
</p>

<p>
<strong>Virtio</strong> is an emerging standard for
paravirtualized drivers, and is particularly used for
QEMU and KVM guests.  Virtio PV drivers are available
for many different operating systems.
</p>

[% END %]

[% WRAPPER h2 h2="Hardware support for virtualization" anchor="hardware" %]

<p>
Nowadays most computer processors contain extra support to accelerate
virtualization (see <a href="../check-hardware-virt/">this tutorial to
check if yours has support and to find out more about the different
types</a>).
</p>

<p>
Support in hardware is often called <strong>hardware
virtualization</strong> (or <strong>hardware virt</strong>),
abbreviated to <strong>HVM</strong>.
</p>

[% END %]

[% WRAPPER h2 h2="Lifecycle" anchor="lifecycle" %]

<p>
Virtual machines can be started and stopped, and you may see this
being referred to as the <strong>lifecycle</strong>
or <strong>lifecycle management</strong>.  This is entirely equivalent
to switching a real machine on and off.
</p>

<p>
As well as just being started and stopped (and unlike real machines),
virtual machines can be <strong>paused</strong>,
<strong>snapshotted</strong> and <strong>cloned</strong>.
</p>

<p>
Taking a snapshot usually means to save the state of the
virtual machine at a moment in time, either so it can be
backed up consistently, or so that you can try some experimental
operation and roll back to the snapshot if that fails.
</p>

<p>
Cloning is somewhat similar to snapshotting, but the two
clones are allowed to run independently of each other
(and cannot be combined again), so that whereas before you
had one virtual machine, now you have two.
</p>

<p>
Another aspect of lifecycle management is <strong>saving</strong>
and <strong>restoring</strong> virtual machines.
</p>

[% END %]

[% WRAPPER h2 h2="Disks and storage" anchor="storage" %]

<p>
The hard drive of a virtual machine is usually emulated ie.  it's not
a physical, separate hard drive, but a file or logical volume on the
host.  This host file (or however it is stored) is called
the <strong>disk image</strong>.
</p>

<p>
Disk images are allocated in <strong>volumes</strong>
out of <strong>storage pools</strong>.
</p>

[% END %]

[% WRAPPER h2 h2="Migration" anchor="migration" %]

<p>
<strong>Migration</strong> means moving a virtual machine from
one host to another.  Usually this means <strong>live migration</strong>
where the running VM is moved without interruption.
</p>

<p>
Don't confuse migration and conversion (see next section).
</p>

[% END %]

[% WRAPPER h2 h2="P2V, V2V and V2P" anchor="p2v" %]

<p>
The terms <strong>P2V</strong>,
<strong>V2V</strong>, and <strong>V2P</strong> refer to different
sorts of <strong>conversions</strong>:
</p>

<table>
<tr>
<th> P2V </th>
<td> Physical to virtual: Converting a physical machine to a
virtual machine.  This is often done so you can continue to
use the applications on the physical machine, but with newer
hardware underneath. </td>
</tr>
<tr>
<th> V2V </th>
<td> Virtual to virtual: Converting a virtual machine
from one hypervisor technology to another, for example
converting a VMWare virtual machine to run on KVM to
save money. </td>
</tr>
<tr>
<th> V2P </th>
<td> Virtual to physical: This is the reverse of P2V, taking
a virtual machine and restoring it onto a physical machine. </td>
</tr>
</table>

[% END %]

[% END -%]
