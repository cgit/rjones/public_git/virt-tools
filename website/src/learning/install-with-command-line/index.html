[% topdir = "../.." -%]
[% PROCESS globals -%]
[% WRAPPER page
   title = "Installing a virtual machine using virt-install"
   h1 = "Installing a virtual machine using virt-install"
   section = "learning"
   local_stylesheet = 1
%]

<p>
Creating a virtual machine with virt-install involves the same steps
as doing it <a href="../start-install-with-virt-manager/">graphically
with virt-manager</a>.
</p>

<p>
First you need to download an ISO of the
operating system you want to install or you can do a
<a href="#network">network install</a>.
</p>

[% WRAPPER h2 h2="Create space for the virtual disk" anchor="disk" %]

<p>
There are several decisions you have to make about how
and where you want to create the virtual hard disk for
the new guest:
</p>

<ol>
<li> <b>How large?</b>
  Decide how large you want the disk image to be.  This is the
  maximum amount of disk space that the virtual machine will be
  able to use, unless you take manual steps to increase it
  (using <code>virt-resize</code>), so choose this wisely.
  Even if you choose a format which <q>grows</q> (like sparse or
  qcow2), this is a hard limit that you cannot easily grow beyond.
  Below are some rough guides for minimum <i>operating system only</i>
  space.  Remember if you want to store significant amounts of
  data in the guest, you must add that requirement on top of
  these figures.
  <table>
  <tr>
  <th> Minimal </th>
  <td> eg. Debian or FreeBSD, text only, configured with just the
    essential components. </td>
  <td> 2048&nbsp;MB </td>
  </tr>
  <tr>
  <th> Linux </th>
  <td> Typical Linux distribution, base install with graphical
    components. </td>
  <td> 8192&nbsp;MB </td>
  </tr>
  <tr>
  <th> Windows </th>
  <td> Recent version of Windows. </td>
  <td> 16384&nbsp;MB </td>
  </tr>
  </table>
  An example calculation: a Linux virtual machine for developers
  &mdash; 8192 (base OS) + 1000 (compiler tools) + 20000 (space to check
  out and compile software in home directory) = 29192&nbsp;MB.
  </li>
<li> <b>File, logical volume or SAN?</b>
  Storing the disk as a plain host file is the easiest option.  As
  long as you have enough free space, you can put the file anywhere,
  and move it around later.  However file access isn't very
  fast, so for the best performance you should choose an LVM2 logical
  volume (LV) or (if you have it) a dedicated LUN on your SAN storage.
  Using a host partition or host disk directly is not recommended
  for security reasons.
  </li>
<li> <b>Fully allocated or sparse?</b>
  If you choose <q>file</q> then you also have the option of using
  a sparse file.  Sparse files are allocated on demand (as the guest
  writes to them), which means you don't need to dedicate disk space
  up-front.  On the other hand, sparse allocation is slow, and
  if you allow the host to run out of disk space this can cause dangerous
  data loss in the guest.
  </li>
<li> <b>Raw or qcow2?</b>
  Again for file storage, you can choose a raw disk (just a massive
  file which is a direct image of what is in the guest's disk), or
  the more flexible <a href="http://en.wikipedia.org/wiki/Qcow2">qcow2
  format</a>.  qcow2 images grow on-demand (a bit like sparse files),
  and there are other features like snapshotting.  However out of all
  the file options, qcow2 is the slowest.
  </li>
<li> <b>Snapshot?</b>
  If you chose LV, qcow2 or SAN, then you might want to clone an
  existing guest.  Snapshots are out of scope for this document.
  For more information we suggest reading the <code>lvcreate(8)</code>
  and/or <code>qemu-img(1)</code> man pages.
  </li>
<li> <b>libvirt storage pools?</b>
  Libvirt now features storage management, and you can store your
  VM disk images in a volume in a libvirt storage pool.  This is out
  of scope for the current document, but we suggest
  <a href="http://libvirt.org/storage.html">reading about libvirt storage
  management here</a>.
  </li>
</ol>

<p>
With these decisions made, you can now go ahead and create the
storage for the guest.  The examples below all assume that the
disk will be 8192 MB in size (as the hard upper limit).  Adjust
the number as required.
</p>

<p>
To create a fully-allocated (non-sparse) raw file:
</p>

<pre>
dd if=/dev/zero of=/var/lib/libvirt/images/guest.img bs=1M count=8192
</pre>

<p>
or for newer versions of Linux, use the faster <code>fallocate(1)</code>
program:
</p>

<pre>
fallocate -l 8192M /var/lib/libvirt/images/guest.img
</pre>

<p>
To create a sparse raw file:
</p>

<pre>
rm -f /var/lib/libvirt/images/guest.img
truncate --size=8192M /var/lib/libvirt/images/guest.img
</pre>

<p>
To create a qcow2 file:
</p>

<pre>
qemu-img create -f qcow2 /var/lib/libvirt/images/guest.qcow2 8192
</pre>

<p>
To create an LVM2 logical volume in the volume group
called <code>vg_host</code>:
</p>

<pre>
lvcreate -n lv_guest -L 8192M /dev/vg_host
</pre>

<p>
SAN LUN creation depends on your SAN, and you should consult that
documentation.
</p>

<p>
To create a libvirt volume in the <code>default</code>
storage pool, do:
</p>

<pre>
virsh vol-create-as default guest 8192M
</pre>

<p>
The libvirt <code>default</code> storage pool is a directory
<code>/var/lib/libvirt/images</code>, and you'll find the disk image
under there.  <code>virsh vol-create-as</code> has several other
options, and you might want to consult the <code>virsh(1)</code> man
page.
</p>

[% END %]

[% WRAPPER h2 h2="Create the virtual machine" anchor="create-vm" %]

<p>
Now you can create the virtual machine itself from the ISO
which you downloaded and the disk image that you created.
</p>

<p>
This is the basic virt-install command:
</p>

<pre>
virt-install -r 1024 --accelerate -n Fedora14 \
  -f /path/to/guest.img \
  --cdrom Fedora-14-x86_64-Live.iso
</pre>

<p>
The <code>-r</code> option specifies the amount of RAM (in megabytes).
This depends on the operating system, but 768&nbsp;MB is a good
starting point these days, and I use 1024&nbsp;MB for modern
graphical Linux and Windows guests.
</p>

<p>
<code>--accelerate</code> indicates you want to use hardware
acceleration.  Recent versions of virt-install default to this.
</p>

<p>
<code>-n</code> specifies the name of the virtual machine
(as known to libvirt), and this is the name you will see
in listings and use when <a href="../start-stop-vm-with-command-line/">starting
and stopping the VM</a>.
</p>

<p>
<code>-f</code> is the full path to the disk image you created before.
For LVs, use the device path,
eg. <code>-f&nbsp;/dev/vg_host/lv_guest</code>
</p>

<p>
<code>--cdrom</code> is the path to the ISO file that you downloaded.
The ISO is only needed during installation, and can be deleted
after that.
</p>

<p>
Other virt-install options that might be useful (read
<code>virt-install(1)</code> for the full list) include:
</p>

<ul>
<li> <code>--vcpus=N</code>
  Specify an SMP guest with N virtual CPUs.
  </li>
<li> <code>--description</code>
  Give a description string which appears in the libvirt XML.
  </li>
<li> <code>-l</code>
  Use this to install from a network URL (instead of needing to
  download an ISO).  See <a href="#network">network installs below</a>.
  </li>
<li> <code>--disk</code>
  This option lets you specify other aspects of the disk such as
  the format (qcow2 instead of raw).  See the man page for the
  full details.
  </li>
<li> <code>--soundhw ac97</code>
  Give the guest a (virtual) AC'97 soundcard.  Without this option
  no soundcard is provided for the guest.
  </li>
</ul>

[% END %]

[% WRAPPER h2 h2="Advanced topic: Installing over the network" anchor="network" %]

<p>
Instead of downloading the ISO, you can install from public
repositories over HTTP.  To do this, remove the <code>--cdrom</code>
option and instead specify the
<code>-l URL</code> option.  Some common locations that virt-install
knows how to handle:
</p>

<ul>
<li> <b>Fedora:</b>
  <code>-l&nbsp;http://download.fedoraproject.org/pub/fedora/linux/releases/13/Fedora/i386/os/</code>
  Change <q>13</q> to the Fedora release, and <q>i386</q>
  to <code>x86_64</code> for a 64 bit guest.
  </li>
<li> <b>Debian:</b>
  <code>-l&nbsp;http://ftp.us.debian.org/debian/dists/stable/main/installer-i386/</code>
  Change <q>us</q> to your country code (for faster access to a local
  mirror), and <q>i386</q> to <code>amd64</code> for a 64 bit guest.
  </li>
<li> <b>Ubuntu:</b>
  <code>-l&nbsp;http://ftp.ubuntu.com/ubuntu/dists/maverick/main/installer-i386/</code>
  Change <q>maverick</q> to the name of the version of Ubuntu to
  install, and <q>i386</q> to <code>amd64</code> for a 64 bit guest.
  </li>
</ul>

[% END %]

[% END -%]
