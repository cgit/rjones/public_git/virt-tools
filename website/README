This directory contains the source for the virt tools website.  Please
read this file carefully before making modifications.

Overview
----------------------------------------------------------------------

The website has four different URLs.  The first is the canonical one,
the other three redirect to the first.

  http://virt-tools.org        # canonical
  http://virttools.org         # redirect
  http://virt-tools.com        # redirect
  http://virttools.com         # redirect

Sub-directories
----------------------------------------------------------------------

- src: Source for the website.

  At the moment we use the perl Template Toolkit to provide simple
  includes, and apart from that it's just flat HTML.  We don't yet use a
  CMS although we are considering using one in future.

- templates: Templates used to build the site.

- build: Preview / staging directory.

To rebuild (preview) the site, run:

  make

and point your browser in the build/ directory:

  firefox build/index.html

To sync the contents of build/ to the live site, run:

  make go-live

Content and stylesheets
----------------------------------------------------------------------

Each page of the site corresponds to a subdirectory (this is so that
'.html' extensions don't appear in URLs).  To create a new page, copy
an existing subdirectory.

Unfortunately this means when previewing that internal links will take
you to a directory listing, but they will work OK on the live site.

One benefit however is you can put all related files in the same
directory (eg. images, page-local stylesheet).

Global stylesheets are in the src/css/ subdirectory.

Some rules:

- Do not mix styling and HTML.  Use simple, small HTML and style it
  via stylesheets.  Don't use table layouts, HTML editorcrap etc.
  Look at existing pages for clues.  In other words, use your head.

- Use relative paths so that pages can be previewed without copying
  them to the live site.

- For page-specific styles, create 'style.css' in the same directory
  containing the page (eg. foo/index.html => foo/style.css), and set
  the local_stylesheet variable in the wrapper.

- Links must *not* contain 'index.html'.  Links *must* end with a '/'
  character.  In general use: <a href="[% topdir %]/foo/">.....</a>

We declare pages XHTML 1.0 Strict to avoid quirks modes, but in fact
our pages are not strict XHTML.  However it's still a good idea to
fully close tags and generally make the HTML as strict as possible.

Templates
----------------------------------------------------------------------

Each page starts with a 'page wrapper'.  Have a look at templates/page
to see the page template.

The variables that you can use include:

- topdir: the relative path to the top directory ("." for the home
  page, ".." for pages which are one subdirectory deep, "../.."  for
  pages which are two subdirectories deep, etc.)

- title: the page title

- h1: the page <h1>, should probably be the same as the title, but not
  too long

- local_stylesheet: If set, this causes a local "style.css" to be
  included for page-specific styles.

- section: This controls which section menu item is highlighted.  See
  templates/page for the full list of sections.
