#!/bin/sh -

rsync -av --delete --exclude=.\* --exclude=#\* --exclude=\*~ \
  build/* \
  et.redhat.com:/var/www/sites/virt-tools.et.redhat.com
