#!/bin/sh -

ttree=/usr/bin/ttree
conf=templates/ttree.conf

[ -x "$ttree" ] || {
    echo "'ttree' is missing, you need to install the 'perl-Template-Toolkit' package"
    exit 1
}

[ -f "$conf" ] || {
    echo "configuration file missing, you are running this from the wrong directory"
    exit 1
}

"$ttree" -f "$conf"
